#!/usr/bin/env python3
import sys

if len(sys.argv) > 1:
    DOCKERFILE=sys.argv[1]
else:
    sys.exit("[e] Usage: "+sys.argv[0]+"[Dockerfile path]")

if 'privileged: true' in open(DOCKERFILE).read():
    sys.exit("[e] Docker runs in privileged mode")
